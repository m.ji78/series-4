package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {
    List<Node> nodes;
    Map<String, String> attributes;
    String inside;
    String tag;
    boolean self;

    public Node() {
        this.nodes = new ArrayList<>();
        this.attributes = new HashMap<>();
        this.inside = "";
        self = false;
        tag = "";
    }

       /*
     * this function will return all that exists inside a tag
     * for example for <html><body><p>hi</p></body></html>, if we are on
     * html tag this function will return <body><p1>hi</p1></body> and if we are on
     * body tag this function will return <p1>hi</p1> and if we are on
     * p tag this function will return hi
     * if there is nothing inside tag then null will be returned
     */

    @Override
    public String getStringInside() {
        if (inside.trim() == "")
            return null;
        return inside;
    }

    /*
     *
     */
    @Override
    public List<Node> getChildren() {
        return nodes;
    }

    /*
     * in html tags all attributes are in key value shape. this function will get a attribute key
     * and return it's value as String.
     * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }

    public String getAllAtribiuts() {
        String result = " ";
        for (String s : attributes.keySet()) {
            result += s + " =\"" + attributes.get(s) + "\" ";
        }
        if (attributes.size() == 0)
            return  "";
        return result;
    }

    @Override
    public String toString() {
        if (tag.equals("br"))
            return "</br></br>";
        if (self)
            return "</" + tag + getAllAtribiuts() + ">";
        inside = inside.replaceAll("\n", "");
        String result = "<" + tag + getAllAtribiuts() + ">";

        for (int i = 0; i < nodes.size(); i++) {
            String child = nodes.get(i).toString();
            child = child.replaceAll("\n", "\n\t");
            if (child.lastIndexOf("\t")!= -1)
            child = child.substring(0, child.lastIndexOf('\t')) + child.substring(child.lastIndexOf('\t') + 1);
            result += "\n" + child;
        }
        return result + "\n" + inside + "\n</" + tag + ">";
    }
}
