package sbu.cs.parser.html;

public class HTMLParser {
    /*
     * this function will get a String and returns a Dom object
     */

    public static Node parse(String document) {
        String tagName = "";
        Node node = new Node();
        int i = 0, index;

        i = document.indexOf("<");
        if (i == -1)
            return null;
        if (document.charAt(i) == '<') {
            document = document.substring(i);
            index = i;
            i = document.indexOf('>');

            if (document.charAt(index + 1) == '/') {
                index += 2;
                tagName = document.substring(index, document.indexOf(" "));
                i = document.indexOf(">");
                parseAttributes(node, document.substring(index, i));
                node.self = true;
                document = document.substring(i + 1);

            } else {
                index = document.indexOf(" ") < document.indexOf(">") ? document.indexOf(" ") : document.indexOf(">");
                tagName = document.substring(1, index);
                parseAttributes(node, document.substring(index, i));

                index = i;

                i = element(document, index, tagName);

                childParser(node, document.substring(index + 1, i));

            }
        }

        node.tag = tagName;
        return node;
    }

    public static void childParser(Node node, String document) {
        int i = 0, index;

        i = document.indexOf("<");

        while (i != -1) {
            String tagName;
            i = document.indexOf("<");

            if (i == -1)
                break;
            if (document.charAt(i) == '<') {
                document = document.substring(i);
                index = 0;
                i = document.indexOf('>');

                if (document.charAt(index + 1) == '/') {
                    index += 2;

                    tagName = document.substring(index, document.indexOf(" "));
                    if (tagName.indexOf("br>") ==0){
                        tagName = "br";
                        i = document.indexOf(">",document.indexOf(">"));
                        document = document.substring(i+1);
                        Node child = new Node();
                        child.tag = tagName;
                        node.nodes.add(child);
                        continue;
                    }
                    Node child = new Node();
                    child.tag = tagName;

                    i = document.indexOf(">");
                    parseAttributes(child, document.substring(index, i));

                    document = document.substring(i + 1);
                    child.self = true;
                    node.nodes.add(child);
                } else {
                    index = document.indexOf(" ") < document.indexOf(">") ? document.indexOf(" ") : document.indexOf(">");
                    tagName = document.substring(1, index);


                    parseAttributes(node, document.substring(index, i));

                    i = element(document, index, tagName);

                    i = document.indexOf(">", i);
                    Node nodeChild = parse(document.substring(0, i + 1));

                    if (nodeChild != null && nodeChild.tag.equals("b")) {
                        node.inside = "<b>" + nodeChild.inside + "</b>";
                    }
                    node.nodes.add(nodeChild);



                    if (i + 1 >= document.length()) {
                        return;
                    } else
                        document = document.substring(i + 1);
                }
            }
        }
        node.inside += document;
    }

    public static int element(String doc, int index, String nameTag) {
        int counter = 1;
        while (counter != 0) {
            index++;
            if (index > 350) {
                //System.out.println(doc.substring(index));
                //System.out.println(doc.substring(index).indexOf("</" + nameTag));
            }
            if (doc.substring(index).indexOf("<" + nameTag) == 0) {
                counter++;
            }
            if (doc.substring(index).indexOf("</" + nameTag) == 0) {
                counter--;
                if (counter == 0)
                    return index;

            }
        }
        return index;
    }

    public static void parseAttributes(Node node, String doc) {
        String key, value;
        int space, valueStart, valueEnd, i;

        if (doc.length() < 2)
            return;

        i = doc.indexOf("=");
        while (i != -1) {
            space = doc.substring(0, i).lastIndexOf(" ");

            valueStart = doc.indexOf("\"", i);
            valueEnd = doc.indexOf("\"", valueStart + 1);

            key = doc.substring(space + 1, i);
            value = doc.substring(valueStart + 1, valueEnd);

            node.attributes.put(key, value);

            doc = doc.substring(valueEnd + 1);
            i = doc.indexOf("=");
        }
    }

    /*
     * a function that will return string representation of dom object.
     * only implement this after all other functions been implemented because this
     * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        return  root.toString();
    }
}
