package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class JsonArray implements JsonElement {
    ArrayList value;

    public JsonArray(ArrayList value) {
        this.value = value;
    }

    public ArrayList getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JsonArray jsonArray = (JsonArray) o;
        return Objects.equals(value, jsonArray.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
