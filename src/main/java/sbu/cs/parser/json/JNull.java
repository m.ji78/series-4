package sbu.cs.parser.json;

public class JNull implements JsonElement {
    @Override
    public String toString() {
        return "null";
    }
}
