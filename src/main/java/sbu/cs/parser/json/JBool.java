package sbu.cs.parser.json;

import java.util.Objects;

public class JBool implements JsonElement {
    boolean value;

    public JBool(boolean value) {
        this.value = value;
    }

    public boolean isValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JBool jBool = (JBool) o;
        return value == jBool.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
