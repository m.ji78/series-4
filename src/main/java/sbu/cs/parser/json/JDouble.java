package sbu.cs.parser.json;

import java.util.Objects;

public class JDouble implements JsonElement {
    Double value;

    public JDouble(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JDouble jDouble = (JDouble) o;
        return Objects.equals(value, jDouble.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
