package sbu.cs.parser.json;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonParser {
    /*
     * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        JsonElement je;
        Json json = new Json();
        data = data.trim();
        data = data.substring(1, data.length() - 1);

        int i = 0, index = 0;
        String key, val;

        data = data.trim();
        i = data.indexOf(":");

        while (i != -1) {
            data = data.trim();

            if (data.charAt(i) == ':') {

                key = data.substring(0, i).trim();
                data = data.substring(i + 1).trim();

                i = 0;
                index = i;
                if (data.charAt(i) == '[') {
                    index = i;
                    i = findJsonArr(data, i);

                    ArrayList res = readJsonArray(data.substring(index + 1, i).trim());

                    je = new JsonArray(res);
                    Pair p = new Pair(key.substring(1, key.length() - 1), je);
                    json.pairList.add(p);


                } else if (data.charAt(i) == '{') {
                    index = i;
                    i = findJsonObj(data, i);

                    Json j = JsonParser.parse(data.substring(index, i + 1));
                    Pair p = new Pair(key.substring(1, key.length() - 1), j);
                    json.pairList.add(p);

                } else {
                    i = data.indexOf(",", i + 1);

                    if (i == -1)
                        val = data;
                    else
                        val = data.substring(index, i).trim();


                    je = readJson(val);
                    Pair p = new Pair(key.substring(1, key.length() - 1), je);
                    json.pairList.add(p);
                }
            }

            data = data.substring(i + 1).trim();
            if (data.charAt(0) != '\"' && data.indexOf("\"") != -1) {
                data = data.substring(data.indexOf("\"")).trim();
            }
            i = data.indexOf(":");
        }
        return json;
    }

    private static JsonElement readJson(String data) {
        int start, end;

        Pattern string = Pattern.compile("\\s*\"([^\"]*)\"\\s*");
        Matcher matchStr = string.matcher(data);

        Pattern nums = Pattern.compile("\\s*\\-*\\d+");
        Matcher matchNum = nums.matcher(data);

        Pattern numDouble = Pattern.compile("\\s*(\\.\\d+)");
        Matcher matchDouble = numDouble.matcher(data);

        Pattern bool1 = Pattern.compile("\\s*true\\s*");
        Matcher matchBool1 = bool1.matcher(data);

        Pattern bool2 = Pattern.compile("\\s*false\\s*");
        Matcher matchBool2 = bool2.matcher(data);

        if (matchStr.find()) {
            start = matchStr.start();
            end = matchStr.end();
            JString jString = new JString(data.substring(start + 1, end - 1).trim());
            return jString;
        } else if (matchNum.find()) {
            start = matchNum.start();
            end = matchNum.end();
            JInteger jInteger = new JInteger(Integer.parseInt(data.substring(start, end).trim()));
            return jInteger;
        } else if (matchDouble.find()) {
            start = matchDouble.start();
            end = matchDouble.end();
            JDouble jDouble = new JDouble(Double.parseDouble(data.substring(start, end).trim()));
            return jDouble;
        } else if (matchBool1.find()) {
            start = matchBool1.start();
            end = matchBool1.end();
            JBool jBool = new JBool(Boolean.parseBoolean(data.substring(start, end).trim()));
            return jBool;
        } else if (matchBool2.find()) {
            start = matchBool2.start();
            end = matchBool2.end();
            JBool jBool = new JBool(Boolean.parseBoolean(data.substring(start, end).trim()));
            return jBool;
        }

        return null;
    }

    public static ArrayList readJsonArray(String data) {
        ArrayList arr = new ArrayList();
        int temp, i = 0, start = 0, end = 0;
        boolean startFlag = true;

        while (i != -1) {
            if (!startFlag) {
                i++;
            }

            data = data.substring(i).trim();
            i = 0;

            if (data.charAt(i) == '{') {
                temp = i;
                i = findJsonObj(data, i);

                Json j = JsonParser.parse(data.substring(temp, i + 1));
                arr.add(j);
            } else if (data.charAt(i) == '[') {
                temp = i;
                i = findJsonArr(data, i);

                ArrayList array = readJsonArray(data.substring(temp, i + 1));
                arr.add(array);

            } else if (data.charAt(i) == '\"') {
                Pattern pattern = Pattern.compile("(\\s*\"([^\"]*)\"\\s*)");
                Matcher matcher = pattern.matcher(data);

                if (matcher.find()) {
                    start = matcher.start();
                    end = matcher.end();
                    String s = data.substring(start + 1, end).trim();
                    s = s.replaceAll("\"", "");
                    arr.add(s);
                }

                data = data.substring(end).trim();

            } else if (data.charAt(i) == 't' || data.charAt(i) == 'f') {
                Pattern pattern1 = Pattern.compile("\\s*true\\s*");
                Matcher matcher1 = pattern1.matcher(data);

                Pattern pattern2 = Pattern.compile("\\s*false\\s*");
                Matcher matcher2 = pattern2.matcher(data);

                if (matcher1.find()) {
                    start = matcher1.start();
                    end = matcher1.end();
                    arr.add(Boolean.parseBoolean(data.substring(start, end).trim()));
                } else if (matcher2.find()) {
                    start = matcher2.start();
                    end = matcher2.end();
                    arr.add(Boolean.parseBoolean(data.substring(start, end).trim()));
                }

                data = data.substring(end).trim();

            } else {
                Pattern pattern = Pattern.compile("(\\-?\\d*\\.?\\d+\\s*)");
                Matcher matcher = pattern.matcher(data);

                if (matcher.find()) {
                    start = matcher.start();
                    end = matcher.end();
                    String s = data.substring(start, end).trim();
                    if (s.contains(".")) {
                        arr.add(Double.parseDouble(s));
                    } else {
                        arr.add(Integer.parseInt(s));
                    }
                }

                data = data.substring(end).trim();

            }

            i = data.indexOf(',');
            startFlag = false;
        }

        return arr;
    }

    public static int findJsonObj(String data, int i) {
        int counter = 1;
        while (counter != 0) {
            i++;
            if (data.charAt(i) == '}') {
                counter--;
                if (counter == 0)
                    break;
            }
            if (data.charAt(i) == '{') {
                counter++;
            }
        }
        return i;
    }

    public static int findJsonArr(String data, int i) {
        int counter = 1;
        while (counter != 0) {
            i++;
            if (data.charAt(i) == ']') {
                counter--;
                if (counter == 0)
                    break;
            }
            if (data.charAt(i) == '[') {
                counter++;
            }
        }
        return i;
    }

    /*
     * this function is the opposite of above function. implementing this has no score and
     * is only for those who want to practice more.
     *
     */
    public static String toJsonString(Json data) {
        return data.toString();
    }


}
