package sbu.cs.parser.json;

import java.util.Objects;

public class JInteger implements JsonElement {
    int value;

    public JInteger(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JInteger jInteger = (JInteger) o;
        return value == jInteger.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
