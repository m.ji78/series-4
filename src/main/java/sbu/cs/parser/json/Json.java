package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface, JsonElement {
    List<Pair<String, JsonElement>> pairList;

    public Json() {
        pairList = new ArrayList<>();
    }

    @Override
    public String getStringValue(String key) {
        for (Pair p : pairList) {
            if (p.first.equals(key)) {
                return p.getSecond();
            }
        }
        return "This key does not exist";
    }

    public Object getValue(String key) {
        for (Pair p : pairList) {
            if (p.first.equals(key)) {
                return p.second;
            }
        }
        return "This key does not exist";
    }

    @Override
    public String toString() {
        JsonElement je;
        String result = "{";
        for (Pair index : this.pairList) {
            if (index.second instanceof JString) {
                je = (JsonElement) index.second;
                result += "\n\t\"" + index.first + "\"" + " : " + "\"" + je.toString() + "\"" + ",";
            } else {
                je = (JsonElement) index.second;
                result += "\n\t\"" + index.first + "\"" + " : " + je.toString() + ",";
            }
        }
        return result.substring(0, result.length() - 1) + "\n}";
    }

}
